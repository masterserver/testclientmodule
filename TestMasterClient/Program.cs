﻿// See https://aka.ms/new-console-template for more information

using System.Net.WebSockets;
using Darari.Serialization.Entities;
using Darari.Serialization.Extensions;
using TestMasterClient;

var address = "ws://localhost:6969/ws";

var ws = new ClientWebSocket();
await ws.ConnectAsync(new Uri(address), CancellationToken.None);
Console.WriteLine(ws.State);

var sendTask = Task.Run(async () =>
{
    while (true)
    {
        var message = Console.ReadLine();
        if (message == "exit")
        {
            break;
        }

        var testMessage = new TestMessage(){Message = message};
        var writer = NetworkWriterPool.Get();
        writer.WriteUInt(0);
        writer.WriteUInt(0);
        testMessage.Serialize(writer);

        await ws.SendAsync(writer.ToArraySegment(), WebSocketMessageType.Binary, true, CancellationToken.None);
    }
});

var receiveTask = Task.Run(async () =>
{
    while (true)
    {
        var buffer = new byte[1024 * 4];
        var receive = await ws.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
        if (receive.CloseStatus.HasValue)
        {
            Console.WriteLine(receive.CloseStatus.Value + ": " + receive.CloseStatusDescription);
            break;
        }
        var reader = NetworkReaderPool.Get(new ArraySegment<byte>(buffer, 0, receive.Count));
        var module = reader.ReadUInt();
        var package = reader.ReadUInt();
        var messagePackage = new TestMessage();
        messagePackage.Deserialize(reader);
        Console.WriteLine(messagePackage.Message);
    }
});

await Task.WhenAll(sendTask, receiveTask);

if (ws.State != WebSocketState.Closed)
{
    await ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "Closing", CancellationToken.None);
}

