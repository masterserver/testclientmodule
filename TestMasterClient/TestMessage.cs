﻿using Darari.Serialization.Abstractions;
using Darari.Serialization.Extensions;

namespace TestMasterClient;

public class TestMessage: ISerializable
{
    public string Message;
    
    public void Serialize(INetworkWriter writer)
    {
        writer.WriteString(Message);
    }

    public void Deserialize(INetworkReader reader)
    {
        Message = reader.ReadString();
    }
}